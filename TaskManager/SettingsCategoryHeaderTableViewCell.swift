//
//  SettingsCategoryHeaderTableViewCell.swift
//  TaskManager
//
//  Created by Antonín Charvát on 09/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class SettingsCategoryHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var addCategoryButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.init(red: 96/255, green: 175/255, blue: 220/255, alpha: 1)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
