//
//  Kategorie.swift
//  TaskManager
//
//  Created by Antonín Charvát on 10/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class Kategorie: NSObject, NSCoding {
    
    var nazev: String?
    var barva: UIColor?
    
    init(nazev: String, barva: UIColor) {
        super.init()
        self.nazev = nazev
        self.barva = barva
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        nazev = aDecoder.decodeObjectForKey("nazev") as? String
        barva = aDecoder.decodeObjectForKey("barva") as? UIColor
    }

    func encodeWithCoder(_aCoder: NSCoder) {
        _aCoder.encodeObject(nazev, forKey: "nazev")
        _aCoder.encodeObject(barva, forKey: "barva")
    }
}
