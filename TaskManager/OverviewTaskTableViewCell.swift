//
//  OverviewTaskTableViewCell.swift
//  TaskManager
//
//  Created by Antonín Charvát on 09/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class OverviewTaskTableViewCell: UITableViewCell {

    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var taskDate: UILabel!
    @IBOutlet weak var splnenoButton: UIButton!
    
    var taskID: String?
    var taskStatus: Bool?
    var notification: Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        splnenoButton.layer.cornerRadius = splnenoButton.frame.size.width / 2
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
