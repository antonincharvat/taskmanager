//
//  EditTaskDateTableViewCell.swift
//  TaskManager
//
//  Created by Antonín Charvát on 10/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class EditTaskDateTableViewCell: UITableViewCell {

    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
