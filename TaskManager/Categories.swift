//
//  Categories.swift
//  TaskManager
//
//  Created by Antonín Charvát on 11/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

struct Categories {
    static let defaultCategories = [Kategorie(nazev: "Osobní", barva: UIColor.blueColor()), Kategorie(nazev: "Pracovní", barva: UIColor.yellowColor()), Kategorie(nazev: "Důležité", barva: UIColor.redColor()), Kategorie(nazev: "Nedůležité", barva: UIColor.grayColor())]
    
    static var customCategories: [Kategorie]?
}
