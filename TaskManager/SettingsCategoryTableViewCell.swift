//
//  SettingsCategoryTableViewCell.swift
//  TaskManager
//
//  Created by Antonín Charvát on 09/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class SettingsCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var categoryView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
        categoryView.layer.cornerRadius = categoryView.frame.size.width / 2
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
