//
//  Task.swift
//  TaskManager
//
//  Created by Antonín Charvát on 09/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import Foundation

class Task {
    
    var id: String
    var nazev: String
    var datum: NSDate
    var kategorie: Kategorie
    var notifikace: Bool
    var splneno: Bool
    
    init(id: String, nazev: String, datum: NSDate, kategorie: Kategorie, notifikace: Bool, splneno: Bool) {
        self.id = id
        self.nazev = nazev
        self.datum = datum
        self.kategorie = kategorie
        self.notifikace = notifikace
        self.splneno = splneno
    }

}
