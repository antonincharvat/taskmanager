//
//  TaskViewController.swift
//  TaskManager
//
//  Created by Antonín Charvát on 09/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class TaskViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    enum Mode {
        case add
        case edit
    }
    var mode: Mode?
    var idToEdit: String?
    
    private var taskToEdit: Task?
    private var editedTaskStatus: Bool?
    
    private var taskName: String?
    private var taskDate: NSDate?
    private var taskCategory: Kategorie?
    private var taskNotification: Bool = true

    let coreDataProvider = CoreDataProvider.sharedInstance
    let localNotificationProvider = LocalNotificationProvider.sharedInstance
    
    private var textField: UITextField?
    private var categoryAlertController: UIAlertController?
    
    private var categories: [Kategorie] = []

    // MARK: Startup
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        let saveButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: #selector(self.saveButtonPressed))
        self.navigationItem.rightBarButtonItem = saveButton
        
        categories = Categories.defaultCategories + Categories.customCategories!
        
        // defaultní hodnota
        taskCategory = categories[0]
        
        if mode == .edit {
            setupEditMode()
        }
    }
    
    // MARK: Edit task name
    func editTaskName() {
        let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? EditTaskTableViewCell
        cell?.label.hidden = true
        textField = UITextField(frame: CGRect(x: 20, y: 0, width: cell!.bounds.width, height: cell!.bounds.height))
        cell?.addSubview(textField!)
        textField!.becomeFirstResponder()
        textField!.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: .EditingChanged)
        
        if mode == .edit {
            textField?.text = taskToEdit?.nazev
        }
    }
    
    func textFieldDidChange(textField: UITextField) {
        self.taskName = textField.text
    }
    
    // MARK: Edit task date
    func datePickerChanged() {
        if (textField != nil && textField!.isFirstResponder()) { textField?.resignFirstResponder() }
        let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as? EditTaskDateTableViewCell
        taskDate = cell?.datePicker.date
    }
    
    // MARK: Edit task category
    func editTaskCategory() {
        if (textField != nil && textField!.isFirstResponder()) { textField?.resignFirstResponder() }
        categoryAlertController = UIAlertController(title: "Vyberte kategorii", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        for category in categories {
            categoryAlertController!.addAction(UIAlertAction(title: category.nazev, style: .Default, handler: { action in
                self.taskCategory = category
                let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 2, inSection: 0)) as? EditTaskTableViewCell
                cell?.label.text = "Kategorie: \(category.nazev!)"
                cell?.categoryView.backgroundColor = category.barva
            }))
        }

        dispatch_async(dispatch_get_main_queue(), { [weak self] in
            self!.presentViewController(self!.categoryAlertController!, animated: true, completion: nil)
        })
    }
    
    // MARK: Edit task notification
    func switchTaskNotification() {
        if (textField != nil && textField!.isFirstResponder()) { textField?.resignFirstResponder() }
        
        let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 3, inSection: 0)) as? EditTaskTableViewCell
        if taskNotification {
            cell?.label.text = "Notifikace: ne"
            taskNotification = false
        }else{
            cell?.label.text = "Notifikace: ano"
            taskNotification = true
        }
    }
    
    func switchMarkAsDone() {
        let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 4, inSection: 0)) as? EditTaskTableViewCell
        if editedTaskStatus == false {
            cell?.label.text = "Označit jako nedokončený"
            editedTaskStatus = true
        }else{
            cell?.label.text = "Označit jako dokončený"
            editedTaskStatus = false
        }
    }

    // MARK: Controls
    func saveButtonPressed() {
        if mode == .add {
            let id = NSUUID().UUIDString
            if taskName != nil {
                if taskDate == nil { taskDate = NSDate() }
                if Settings.notifikace == true {
                    if taskNotification {
                        localNotificationProvider.setLocalNotificationForID(id, withBody: taskName!, forDate: taskDate!)
                    }
                }else{
                    taskNotification = false
                }
                coreDataProvider.saveTaskWithID(id, withName: taskName!, withDate: taskDate!, withCategory: taskCategory!, notify: taskNotification, withStatus: false)
                navigationController?.popViewControllerAnimated(true)
            }else{
                let ac = UIAlertController(title: "Upozornění", message: "Prosím zadejte název tasku", preferredStyle: .Alert)
                ac.addAction(UIAlertAction(title: "Zpět", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(ac, animated: true, completion: nil)
            }
        }else if mode == .edit {
            if taskNotification {
                localNotificationProvider.removeLocalNotificationForID(idToEdit!) // pokud už tam nějaká je, odstraní se
                if editedTaskStatus == false {
                    localNotificationProvider.setLocalNotificationForID(idToEdit!, withBody: taskName!, forDate: taskDate!)
                }
            }else{
                localNotificationProvider.removeLocalNotificationForID(idToEdit!)
            }
            coreDataProvider.updateTaskWithID(idToEdit!, withName: taskName, withDate: taskDate, withCategory: taskCategory, notify: taskNotification, withStatus: editedTaskStatus)
            navigationController?.popViewControllerAnimated(true)
        }
    }
    
    // MARK: Setup for edit mode
    func setupEditMode() {
        let tasks = coreDataProvider.loadTasks()
        for task in tasks[0] {
            if task.id == idToEdit { taskToEdit = task }
        }
        for task in tasks[1] {
            if task.id == idToEdit { taskToEdit = task }
        }
        taskName = taskToEdit?.nazev
        taskDate = taskToEdit?.datum
        taskCategory = taskToEdit?.kategorie
        taskNotification = taskToEdit!.notifikace
        editedTaskStatus = taskToEdit!.splneno
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


// MARK: UITableViewDataSource
extension TaskViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if mode == .add {
            return 4
        }
        return 5
    }
}

// MARK: UITableViewDelegate
extension TaskViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch (indexPath.row) {
        case 0, 2, 3, 4:
            return 70
        case 1:
            return 140
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch (indexPath.row) {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("EditTaskTableViewCell") as? EditTaskTableViewCell
            if mode == .add {
                cell!.label.text = "Klepnutím začněte psát úkol"
            }else if mode == .edit {
                cell!.label.text = taskToEdit?.nazev
            }
            cell!.categoryView.hidden = true
            return cell!

        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("EditTaskDateTableViewCell") as? EditTaskDateTableViewCell
            if mode == .edit {
                cell!.datePicker.setDate(taskToEdit!.datum, animated: false)
            }
            cell!.datePicker.addTarget(self, action: #selector(self.datePickerChanged), forControlEvents: UIControlEvents.ValueChanged)
            return cell!

        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("EditTaskTableViewCell") as? EditTaskTableViewCell
            if mode == .add {
                cell?.label.text = "Kategorie: " + (taskCategory?.nazev)!
                cell?.categoryView.backgroundColor = (taskCategory?.barva)!
            }else if mode == .edit {
                cell?.label.text = "Kategorie: " + (taskToEdit?.kategorie.nazev)!
                cell?.categoryView.backgroundColor = (taskToEdit?.kategorie.barva)!
            }
            return cell!

        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier("EditTaskTableViewCell") as? EditTaskTableViewCell
            if mode == .add {
                if Settings.notifikace == true {
                    cell?.label.text = "Notifikace: ano"
                }else{
                    cell?.label.text = "Notifikace: ne"
                }
            }else if mode == .edit {
                if taskToEdit!.notifikace {
                    cell?.label.text = "Notifikace: ano"
                }else {
                    cell?.label.text = "Notifikace: ne"
                }
            }
            if Settings.notifikace == false {
                cell?.label.textColor = UIColor.grayColor()
                cell?.userInteractionEnabled = false
            }
            cell?.categoryView.hidden = true
            return cell!
            
        case 4:
            let cell = tableView.dequeueReusableCellWithIdentifier("EditTaskTableViewCell") as? EditTaskTableViewCell
            cell?.label.textColor = UIColor.redColor()
            if editedTaskStatus == false {
                cell?.label.text = "Označit jako dokončený"
            }else{
                cell?.label.text = "Označit jako nedokončený"
            }
            cell?.categoryView.hidden = true
            return cell!

        default:
            let defaultCell = tableView.dequeueReusableCellWithIdentifier("default")
            return defaultCell!
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        switch (indexPath.row) {
        case 0:
            editTaskName()
        case 2:
            editTaskCategory()
        case 3:
            switchTaskNotification()
        case 4:
            switchMarkAsDone()
        default:
            break
        }
    }
}

