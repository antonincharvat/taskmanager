//
//  LocalNotificationProvider.swift
//  TaskManager
//
//  Created by Antonín Charvát on 11/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class LocalNotificationProvider: NSObject {
    class var sharedInstance: LocalNotificationProvider {
        struct Static {
            static let instance: LocalNotificationProvider = LocalNotificationProvider()
        }
        return Static.instance
    }
    
    let defaults = NSUserDefaults.standardUserDefaults()
    let coreDataProvider = CoreDataProvider.sharedInstance

    func setLocalNotificationForID(id: String, withBody body: String, forDate date: NSDate) {
        let notification = UILocalNotification()
        notification.alertBody = body
        notification.alertAction = "open"
        notification.fireDate = date
        notification.userInfo = ["UUID": id]
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
    
    func removeLocalNotificationForID(id: String) {
        for notification in UIApplication.sharedApplication().scheduledLocalNotifications! as [UILocalNotification]{
            if (notification.userInfo!["UUID"] as! String == id) {
                UIApplication.sharedApplication().cancelLocalNotification(notification)
            }
        }
    }
    
    func invalidateNotifications(notifications: [UILocalNotification]) {
        for notification in notifications {
            coreDataProvider.saveNotificationWithID(notification.userInfo!["UUID"] as! String, withName: notification.alertBody!, forDate: notification.fireDate!)
            removeLocalNotificationForID(notification.userInfo!["UUID"] as! String)
        }
    }
    
    func validateNotificationsWithIDs(ids: [String]) {
        for id in ids {
            let notification = coreDataProvider.loadNotificationDataForID(id)
            setLocalNotificationForID(notification![0].0, withBody: notification![0].1, forDate: notification![0].2)
            coreDataProvider.deleteNotificationWithID(id)
        }
    }
}
