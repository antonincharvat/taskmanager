//
//  OverviewViewController.swift
//  TaskManager
//
//  Created by Antonín Charvát on 09/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class OverviewViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    let coreDataProvider = CoreDataProvider.sharedInstance
    let localNotificationProvider = LocalNotificationProvider.sharedInstance
    var tasks: [[Task]]?
    let defaults = NSUserDefaults.standardUserDefaults()

    // MARK: Startup
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        Categories.customCategories = coreDataProvider.loadCategories()
        Settings.notifikace = defaults.boolForKey("notifikace")
        Settings.razeni = defaults.integerForKey("razeni")
    }
    
    override func viewWillAppear(animated: Bool) {
        tasks = coreDataProvider.loadTasks()
        tableView.reloadData()
        
        print("Počet neaktivních notifikací: \(coreDataProvider.loadNotificationIDs().count)")
        print("Počet aktivních notifikací: \(UIApplication.sharedApplication().scheduledLocalNotifications?.count)")
        // odkomentuj pro smazání všech notifikací
        //for notification in UIApplication.sharedApplication().scheduledLocalNotifications! {
        //    UIApplication.sharedApplication().cancelLocalNotification(notification)
        //}
    }
    
    // MARK: Controls
    @IBAction func addTask(sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewControllerWithIdentifier("TaskViewController") as? TaskViewController
        vc?.mode = .add
        self.showViewController(vc!, sender: self)
    }

    @IBAction func openSettings(sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewControllerWithIdentifier("SettingsViewController") as? SettingsViewController
        self.showViewController(vc!, sender: self)
    }
    
    func splnenoPressed(sender: UIButton) {
        let contentView = sender.superview
        let cell = contentView?.superview as? OverviewTaskTableViewCell
        let id = cell!.taskID
        let status = cell!.taskStatus
        let notifikace = cell!.notification
        coreDataProvider.updateTaskWithID(id!, withName: nil, withDate: nil, withCategory: nil, notify: nil, withStatus: !status!)
        if notifikace == true {
            if status == false { // odsranění notifikace, pokud existuje
                localNotificationProvider.removeLocalNotificationForID(id!)
            }
        }
        tasks = coreDataProvider.loadTasks()
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: UITableViewDataSource
extension OverviewViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCellWithIdentifier("OverviewHeaderTableViewCell") as? OverviewHeaderTableViewCell
        switch (section) {
        case 0:
            headerCell?.headerLabel.text = "TODO"
            headerCell?.contentView.backgroundColor = UIColor.init(red: 96/255, green: 175/255, blue: 220/255, alpha: 1)
        case 1:
            headerCell?.headerLabel.text = "DONE"
            headerCell?.contentView.backgroundColor = UIColor.init(red: 140/255, green: 175/255, blue: 220/255, alpha: 1)
        default:
            break
        }
        return headerCell?.contentView
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks![section].count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("OverviewTaskTableViewCell") as? OverviewTaskTableViewCell
        let datum = tasks![indexPath.section][indexPath.row].datum
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd.MM.yyyy hh:mm"
        let datumString = formatter.stringFromDate(datum)

        let kategorie = tasks![indexPath.section][indexPath.row].kategorie as Kategorie
        let barva = kategorie.barva
        
        let id = tasks![indexPath.section][indexPath.row].id
        let status = tasks![indexPath.section][indexPath.row].splneno
        let notification = tasks![indexPath.section][indexPath.row].notifikace
        
        cell?.taskStatus = status
        cell?.taskID = id
        cell?.notification = notification
        cell?.splnenoButton.backgroundColor = barva
        cell?.splnenoButton.addTarget(self, action: #selector(self.splnenoPressed(_:)), forControlEvents: .TouchUpInside)
        
        // umístění checkmarku když je task označen jako done
        for subview in cell!.splnenoButton.subviews {
            subview.removeFromSuperview()
        }
        if cell?.taskStatus == true {
            let image = UIImage(named: "check")
            let imageView = UIImageView(image: image)
            imageView.frame = cell!.splnenoButton.bounds
            cell?.splnenoButton.addSubview(imageView)
        }

        cell?.taskName.text = tasks![indexPath.section][indexPath.row].nazev
        cell?.taskDate.text = datumString
        return cell!
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let cell = tableView.cellForRowAtIndexPath(indexPath) as? OverviewTaskTableViewCell
            let id = cell?.taskID
            coreDataProvider.deleteTaskWithID(id!)
            coreDataProvider.deleteNotificationWithID(id!)
            localNotificationProvider.removeLocalNotificationForID(id!)
            tasks = coreDataProvider.loadTasks()
            tableView.reloadData()
        }
    }
}

// MARK: UITableViewDelegate
extension OverviewViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as? OverviewTaskTableViewCell
        let id = cell?.taskID
        let vc = storyboard?.instantiateViewControllerWithIdentifier("TaskViewController") as? TaskViewController
        vc?.mode = .edit
        vc?.idToEdit = id
        self.showViewController(vc!, sender: self)
    }
}