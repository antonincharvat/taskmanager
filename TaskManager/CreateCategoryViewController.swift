//
//  CreateCategoryViewController.swift
//  TaskManager
//
//  Created by Antonín Charvát on 11/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class CreateCategoryViewController: UIViewController {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var stackView: UIStackView!
    
    let colors = [UIColor.greenColor(), UIColor.magentaColor(), UIColor.brownColor(), UIColor.blackColor(), UIColor.orangeColor()]
    
    var nazev: String?
    var barva: UIColor?
    
    let coreDataProvider = CoreDataProvider.sharedInstance
    
    // MARK: Startup
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.becomeFirstResponder()
        textField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        for index in 0...4 {
            stackView.subviews[index].backgroundColor = colors[index]
        }
    }
    
    func textFieldDidChange(textField: UITextField) {
        nazev = textField.text
    }
    
    // MARK: Controls
    @IBAction func colorSelected(sender: UIButton) {
        for subview in stackView.subviews {
            if subview.tag == 10 {
                for subsubview in subview.subviews {
                    subsubview.removeFromSuperview()
                }
            }
        }
        for subview in sender.subviews {
            subview.removeFromSuperview()
        }
        
        barva = sender.backgroundColor
        let image = UIImage(named: "check")
        let imageView = UIImageView(image: image)
        imageView.frame = sender.bounds
        sender.addSubview(imageView)
    }

    @IBAction func saveAndBack(sender: UIButton) {
        if nazev == nil {
            let ac = UIAlertController(title: "Upozornění", message: "Musíte vyplnit název kategorie", preferredStyle: .Alert)
            ac.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            self.presentViewController(ac, animated: true, completion: nil)
        }else if barva == nil {
            let ac = UIAlertController(title: "Upozornění", message: "Musíte vybrat barvu", preferredStyle: .Alert)
            ac.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            self.presentViewController(ac, animated: true, completion: nil)
        }else{
            let category = Kategorie(nazev: nazev!, barva: barva!)
            coreDataProvider.saveCategoryWithName(category.nazev!, withColor: category.barva!)
            Categories.customCategories = coreDataProvider.loadCategories()
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    @IBAction func back(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
