//
//  CoreDataProvider.swift
//  CharvatDemo
//
//  Created by Antonín Charvát on 03/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit
import CoreData

class CoreDataProvider: NSObject {
    class var sharedInstance: CoreDataProvider {
        struct Static {
            static let instance: CoreDataProvider = CoreDataProvider()
        }
        return Static.instance
    }
    
    var appDel: AppDelegate
    var context: NSManagedObjectContext
    var fetchedItems: [NSManagedObject]?
    
    override init() {
        self.appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        self.context = appDel.managedObjectContext
    }

    // MARK: Categories
    func saveCategoryWithName(nazev: String, withColor color: UIColor) {
        let newRecord = NSEntityDescription.insertNewObjectForEntityForName("Kategorie", inManagedObjectContext: self.context)
        
        newRecord.setValue(nazev, forKey: "nazev")
        newRecord.setValue(color, forKey: "barva")
        
        do {
            try context.save()
        } catch {
            print("Error while saving into CoreData: \(error)")
        }
    }
    
    func loadCategories() -> [Kategorie] {
        let request = NSFetchRequest(entityName: "Kategorie")
        request.returnsObjectsAsFaults = false
        fetchedItems = try! context.executeFetchRequest(request) as! [NSManagedObject]
        var results: [Kategorie] = []
        for item in fetchedItems!{
            let nazev = item.valueForKey("nazev") as! String
            let barva = item.valueForKey("barva") as! UIColor
            let kategorie = Kategorie(nazev: nazev, barva: barva)
            results.append(kategorie)
        }
        return results
    }
    
    // MARK: Tasks
    func saveTaskWithID(id: String, withName nazev: String, withDate date: NSDate, withCategory category: Kategorie, notify notification: Bool, withStatus status: Bool) {
        let newRecord = NSEntityDescription.insertNewObjectForEntityForName("Task", inManagedObjectContext: self.context)
        newRecord.setValue(id, forKey: "id")
        newRecord.setValue(nazev, forKey: "nazev")
        newRecord.setValue(date, forKey: "datum")
        newRecord.setValue(category, forKey: "kategorie")
        newRecord.setValue(notification, forKey: "notifikace")
        newRecord.setValue(status, forKey: "splneno")
        
        do {
            try context.save()
        } catch {
            print("Error while saving into CoreData: \(error)")
        }
    }
    
    func loadTasks() -> [[Task]] {
        let request = NSFetchRequest(entityName: "Task")
        request.returnsObjectsAsFaults = false
        fetchedItems = try! context.executeFetchRequest(request) as! [NSManagedObject]
        var results: [Task] = []
        for item in fetchedItems! {
            let id = item.valueForKey("id") as! String
            let nazev = item.valueForKey("nazev") as! String
            let datum = item.valueForKey("datum") as! NSDate
            let kategorie = item.valueForKey("kategorie") as! Kategorie
            let notifikace = item.valueForKey("notifikace") as! Bool
            let splneno = item.valueForKey("splneno") as! Bool
            
            let task = Task(id: id, nazev: nazev, datum: datum, kategorie: kategorie, notifikace: notifikace, splneno: splneno)
            results.append(task)
        }
        var todo: [Task] = []
        var done: [Task] = []
        for task in results {
            if !task.splneno {
                todo.append(task)
            }else{
                done.append(task)
            }
        }
        if Settings.razeni == 0 { // abecedne
            todo = todo.sort({$0.0.nazev < $0.1.nazev})
            done = done.sort({$0.0.nazev < $0.1.nazev})
        }else{ // podle data
            todo = todo.sort({$0.0.datum.compare($0.1.datum) == NSComparisonResult.OrderedAscending})
            done = done.sort({$0.0.datum.compare($0.1.datum) == NSComparisonResult.OrderedAscending})
        }
        return [todo, done]
    }
    
    func updateTaskWithID(id: String, withName nazev: String?, withDate date: NSDate?, withCategory category: Kategorie?, notify notification: Bool?, withStatus status: Bool?) {
        let request = NSFetchRequest(entityName: "Task")
        request.returnsObjectsAsFaults = false
        fetchedItems = try! context.executeFetchRequest(request) as! [NSManagedObject]
        for task in fetchedItems! {
            let fetchedID = task.valueForKey("id") as! String
            if fetchedID == id {
                if let novyNazev = nazev {
                    task.setValue(novyNazev, forKey: "nazev")
                }
                if let novyDatum = date {
                    task.setValue(novyDatum, forKey: "datum")
                }
                if let novaKategorie = category {
                    task.setValue(novaKategorie, forKey: "kategorie")
                }
                if let novaNotifikace = notification {
                    task.setValue(novaNotifikace, forKey: "notifikace")
                }
                if let novyStatus = status {
                    task.setValue(novyStatus, forKey: "splneno")
                }
            }
        }
        
        do {
            try context.save()
        } catch {
            print("Error while saving into CoreData: \(error)")
        }
    }
    
    func deleteTaskWithID(id: String) {
        let request = NSFetchRequest(entityName: "Task")
        request.returnsObjectsAsFaults = false
        fetchedItems = try! context.executeFetchRequest(request) as! [NSManagedObject]
        for task in fetchedItems! {
            let fetchedID = task.valueForKey("id") as! String
            if fetchedID == id {
                context.deleteObject(task)
            }
        }
        
        do {
            try context.save()
        } catch {
            print("Error while saving into CoreData: \(error)")
        }
    }
    
    // MARK: Notifications
    func saveNotificationWithID(id: String, withName name: String, forDate date: NSDate) {
        let newRecord = NSEntityDescription.insertNewObjectForEntityForName("Notifikace", inManagedObjectContext: context)
        newRecord.setValue(id, forKey: "id")
        newRecord.setValue(name, forKey: "nazev")
        newRecord.setValue(date, forKey: "datum")
        
        do {
            try context.save()
        } catch {
            print("Error while saving into CoreData: \(error)")
        }
    }
    
    func loadNotificationDataForID(id: String) -> [(String, String, NSDate)]? {
        let request = NSFetchRequest(entityName: "Notifikace")
        request.returnsObjectsAsFaults = false
        fetchedItems = try! context.executeFetchRequest(request) as! [NSManagedObject]
        var result: [(String, String, NSDate)] = []
        for notification in fetchedItems! {
            if notification.valueForKey("id") as! String == id {
                let nazev = notification.valueForKey("nazev") as! String
                let datum = notification.valueForKey("datum") as! NSDate
                result.append((id, nazev, datum))
            }
        }
        return result
    }
    
    func loadNotificationIDs() -> [String] {
        let request = NSFetchRequest(entityName: "Notifikace")
        request.returnsObjectsAsFaults = false
        fetchedItems = try! context.executeFetchRequest(request) as! [NSManagedObject]
        var ids: [String] = []
        for notification in fetchedItems! {
            let id = notification.valueForKey("id") as! String
            ids.append(id)
        }
        return ids
    }
    
    func deleteNotificationWithID(id: String) {
        let request = NSFetchRequest(entityName: "Notifikace")
        request.returnsObjectsAsFaults = false
        fetchedItems = try! context.executeFetchRequest(request) as! [NSManagedObject]
        for notification in fetchedItems! {
            if notification.valueForKey("id") as! String == id {
                context.deleteObject(notification)
            }
        }
        
        do {
            try context.save()
        } catch {
            print("Error while saving into CoreData: \(error)")
        }
    }
}
