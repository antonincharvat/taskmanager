//
//  SettingsViewController.swift
//  TaskManager
//
//  Created by Antonín Charvát on 09/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var categories: [Kategorie] = []
    let defaults = NSUserDefaults.standardUserDefaults()
    let localNotificationProvider = LocalNotificationProvider.sharedInstance
    let coreDataProvider = CoreDataProvider.sharedInstance
    
    // MARK: Startup
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        if let customCategories = Categories.customCategories {
            categories = Categories.defaultCategories + customCategories
        }else{
            categories = Categories.defaultCategories
        }
        tableView.reloadData()
    }
    
    // MARK: Create category VC
    func addCategory() {
        let vc = storyboard?.instantiateViewControllerWithIdentifier("CreateCategoryViewController") as? CreateCategoryViewController
        self.presentViewController(vc!, animated: true, completion: nil)
    }
    
    // MARK: Temporary invalidate notification
    func invalidateNotifications(invalidate: Bool) {
        if invalidate {
            let notifications = UIApplication.sharedApplication().scheduledLocalNotifications
            localNotificationProvider.invalidateNotifications(notifications!)
        }else{
            let ids = coreDataProvider.loadNotificationIDs()
            localNotificationProvider.validateNotificationsWithIDs(ids)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: UITableViewDataSource
extension SettingsViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCellWithIdentifier("SettingsCategoryHeaderTableViewCell") as? SettingsCategoryHeaderTableViewCell
        cell?.addCategoryButton.addTarget(self, action: #selector(self.addCategory), forControlEvents: .TouchUpInside)
        return cell?.contentView
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count + 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SettingsCategoryTableViewCell") as? SettingsCategoryTableViewCell
        if indexPath.row < (categories.count) {
            cell?.label.text = categories[indexPath.row].nazev
            cell?.categoryView.backgroundColor = categories[indexPath.row].barva
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("SettingsDefaultTableViewCell") as? SettingsDefaultTableViewCell
            if indexPath.row == categories.count {
                if Settings.notifikace == true {
                    cell?.label.text = "Notifikace: zapnuty"
                }else{
                    cell?.label.text = "Notifikace: vypnuty"
                }
            }else{
                if Settings.razeni == 0 {
                    cell?.label.text = "Řazení tasků: dle abecedy"
                }else{
                    cell?.label.text = "Řazení tasků: dle data"
                }
            }
            return cell!
        }
        return cell!
    }
}

// MARK: UITableViewDelegate
extension SettingsViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch (indexPath.row) {
        case categories.count...(categories.count + 2):
            if indexPath.row == categories.count{
                Settings.notifikace = !Settings.notifikace!
                tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: categories.count, inSection: 0)], withRowAnimation: .None)
                if Settings.notifikace == true {
                    invalidateNotifications(false)
                    defaults.setBool(true, forKey: "notifikace")
                }else{
                    invalidateNotifications(true)
                    defaults.setBool(false, forKey: "notifikace")
                }
            }else{
                if Settings.razeni == 0 { Settings.razeni = 1 }
                else if Settings.razeni == 1 { Settings.razeni = 0 }
                tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: categories.count + 1, inSection: 0)], withRowAnimation: .None)
                defaults.setInteger(Settings.razeni!, forKey: "razeni")
            }
        default:
            break
        }
    }
}